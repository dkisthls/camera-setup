alias aocdisp='sudo xrandr --newmode "1360x768_60.00"   84.75  1360 1432 1568 1776  768 771 781 798 -hsync +vsync
sudo xrandr --addmode DVI-I-1-1 "1360x768_60.00"
xrandr --output DVI-I-1-1 --mode "1360x768_60.00"'

alias ssh='ssh -A -Y'

alias cssa='cd ${ATST}/admin/css'
alias cssc='cd ${ATST}/src/c++/atst/css'
alias cssj='cd ${ATST}/src/java/atst/css'
alias cssjt='cd ${ATST}/src/java-test/atst/css'
alias cssp='cd ~/code/python'
alias cssr='cd ${ATST}/resources/screens/css/templates'
alias csst='cd ${ATST}/tools/css'
alias cam='cd ${ATST}/src/c++/atst/base/hardware/camera'

alias cdlog='cd ${ATST_RUN_ENVIRON}/logs/containers; ls -lrt'
alias catc='catLog C'
alias catj='catLog J'
alias catcn='catLog CN'
alias catdl='catLog DL'
alias grepc='grepLog C'
alias grepj='grepLog J'
alias grepcn='grepLog CN'
alias grepdl='grepLog DL'
alias tailc='viewLog tailf C'
alias tailj='viewLog tailf J'
alias tailcn='viewLog tailf CN'
alias taildl='viewLog tailf DL'
alias vic='viewLog vim C'
alias vij='viewLog vim J'
alias vicn='viewLog vim CN'
alias vidl='viewLog vim DL'

alias cmRestart='ContainerManager stop && ContainerManager start'
alias sublime='/opt/sublime_text/sublime_text'

